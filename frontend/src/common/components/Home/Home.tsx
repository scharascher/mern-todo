import React from 'react';
import 'common/components/Home/Home.scss';

const Home: React.FC = () => {
    return (
        <div>
            <h1>Welcome to app</h1>
            <p>App contains todos for users</p>
        </div>
    );
};

export default Home;
